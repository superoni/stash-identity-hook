== Stash Email User Validator Hook ==

This plugin helps prevent users from committing with their vanity email addresses.

This might be useful if you're in a corporate environment that integrated with a centralized user directory.
The plugin tests to see if the user exists in the Stash database by scanning each commit author's email address.

It would be impractical to enforce this on all commits since the beginning of time:
* Users get deleted
* You've got legacy commits that you do not want to revise

A grace period is built-in that only inspects commits within the last X days. Default as of this writing is seven days.
This is configurable within the hook settings.
