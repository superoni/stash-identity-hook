package example.superoni.stash.hooks;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.hook.HookResponse;
import com.atlassian.stash.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.RefChangeType;
import com.atlassian.stash.history.HistoryService;
import com.atlassian.stash.user.DetailedUser;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.atlassian.stash.user.StashUser;
import com.atlassian.stash.user.UserService;
import com.atlassian.stash.util.Page;
import com.atlassian.stash.util.PageRequest;
import com.atlassian.stash.util.PageRequestImpl;

import javax.annotation.Nonnull;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

public class EmailValidateHook extends AbstractBaseHook implements BaseHook, PreReceiveRepositoryHook
{
    private final ApplicationProperties applicationProperties;
    private final HistoryService historyService;
    private final UserService userService;
    private final StashAuthenticationContext stashAuthenticationContext;
    public final static int MAX_REFS_TO_TEST = 100;


    public EmailValidateHook(ApplicationProperties applicationProperties, HistoryService historyService, UserService userService,
                       StashAuthenticationContext stashAuthenticationContext)
    {
        this.applicationProperties = applicationProperties;
        this.historyService = historyService;
        this.userService = userService;
        this.stashAuthenticationContext = stashAuthenticationContext;

    }



    public String getName()
    {
        if(null != applicationProperties)
        {
            return "myComponent:" + applicationProperties.getDisplayName();
        }

        return "myComponent";
    }

    public boolean isValidUser(String email){
        // findUsersByName searches display name, email, and usernames
        StashUser user = userService.findUserByNameOrEmail(email);
        if (user != null){
            return true;
        }
        return false;
    }


    @Override
    public boolean onReceive(@Nonnull RepositoryHookContext repositoryHookContext, @Nonnull Collection<RefChange> refChanges, @Nonnull HookResponse hookResponse) {

        boolean failNameTest = false;

        for (RefChange refChange : refChanges) {
            // Skip checking on branch tag creation / deletion
            if (refChange.getType().equals( RefChangeType.ADD) || refChange.getType().equals( RefChangeType.DELETE )){
                return true;
            }

            Page<Changeset> changesets = historyService.getChangesetsBetween(
                    repositoryHookContext.getRepository(), refChange.getFromHash(), refChange.getToHash(), new PageRequestImpl(0, 20));

            for (Iterable<Changeset> currentPageChangeset = changesets.getValues(); changesets.getSize() >= 1; changesets = historyService.getChangesetsBetween(
                    repositoryHookContext.getRepository(), refChange.getFromHash(), refChange.getToHash(), changesets.getNextPageRequest())){

                if (changesets.getSize() > MAX_REFS_TO_TEST){
                    // This shouldn't happen, but if it does, let the operation continue.
                    hookResponse.err().println("Exceeded maximum refs (" + MAX_REFS_TO_TEST + ") to test -- bypassing hook " +
                            getName());
                    return true;
                }

                for (Changeset changeset : currentPageChangeset){
                    if (isAfterGraceDate(changeset, repositoryHookContext.getSettings().getString("graceDate")) )
                    {
                        if (! isValidUser(changeset.getAuthor().getEmailAddress()) ){
                            failNameTest = true;
                            hookResponse.err().println("Invalid Author: " + changeset.getId() + " : (" + changeset.getAuthor().getName() +
                                    " <" + changeset.getAuthor().getEmailAddress() + ">) " +
                                    changeset.getMessage() + " : " + refChange.getRefId());
                        }

                    }

                }

                if (changesets.getNextPageRequest() == null){
                    break;
                }
            }

        }

        if (failNameTest){
            hookResponse.err().println("\nThis git repository does not allow invalid user names.\n" +
                "Correct your git configuration and amend your commits.");
            return false;
        }
        return true;
    }
}