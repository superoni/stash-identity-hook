package example.superoni.stash.hooks;

import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.setting.RepositorySettingsValidator;
import com.atlassian.stash.setting.Settings;
import com.atlassian.stash.setting.SettingsValidationErrors;

import javax.annotation.Nonnull;

public class DateValidator implements RepositorySettingsValidator {

    @Override
    public void validate(@Nonnull Settings settings, @Nonnull SettingsValidationErrors settingsValidationErrors, @Nonnull Repository repository) {
        String graceDate = settings.getString("graceDate", null);
        if (graceDate != null && !graceDate.isEmpty()) {
            try{
                int days = Integer.parseInt(graceDate);

                if (days < 1){
                    settingsValidationErrors.addFieldError("graceDate", "1 or more days must be specified. If empty, the default will be used. Defaut=" + EmailValidateHook.DEFAULT_GRACE_DAYS);
                }
            }
            catch(NumberFormatException e){
                settingsValidationErrors.addFieldError("graceDate", "You must specify a number in days. If empty, the default will be used. Defaut=" + EmailValidateHook.DEFAULT_GRACE_DAYS);

            }
        }
    }
}
