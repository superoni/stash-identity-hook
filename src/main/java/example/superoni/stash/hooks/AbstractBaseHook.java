package example.superoni.stash.hooks;

import com.atlassian.stash.content.Changeset;

import java.util.Date;

public abstract class AbstractBaseHook implements BaseHook {
    public final static int DEFAULT_GRACE_DAYS = 7;

    public boolean isAfterGraceDate(Changeset changeset, String graceDateSetting){
        Date authorTimestamp = changeset.getAuthorTimestamp();

        if (graceDateSetting == null || graceDateSetting.isEmpty()){
            graceDateSetting = String.valueOf(DEFAULT_GRACE_DAYS);
        }
        long DAY_IN_MS = 1000 * 60 * 60 * 24;

        Date graceDate = new Date(System.currentTimeMillis() - (Integer.parseInt(graceDateSetting) * DAY_IN_MS));
        if (authorTimestamp.after(graceDate)){
            return true;
        }
        return false;

    }
}
