package example.superoni.stash.hooks;

public interface BaseHook
{
    String getName();
}